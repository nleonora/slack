# Slack

This is an ESS Gem for slack.
For now it's just a simple utility for sending messages to the slack api

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'slack', git: 'git@bitbucket.org:llusdaer/slack.git'
```

And then execute:

    $ bundle install

## Usage

```
ss = Slack::Sender.new( slack_bot_or_user_token, 
                        slack_channel )
ss.post("enc2 overhead1")
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
