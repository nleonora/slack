require "test_helper"

class SlackTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Slack::VERSION
  end

  def test_it_does_something_useful
    ss = Slack::Sender.new(nil,nil)
    response = ss.post("hello")
    refute JSON.parse(response.body)["ok"]
  end
end
