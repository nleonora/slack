require 'net/http'
require 'json'

module Slack
  class Sender
    def initialize(user_token, channel, url="https://slack.com/api/chat.postMessage")
      @user_token = user_token
      @channel = channel
      @url = url
    end
  
    def curl(msg)
      cmd = []
      cmd << "curl -X POST"
      cmd << "-H 'Authorization: Bearer #{@user_token}'"
      cmd << "-H 'Content-type: application/json'"
      cmd << "--data '{\"channel\":\"#{@channel}\",\"text\":\"#{msg}\"}'"
      cmd << @url
  
      `#{cmd.join(" ")}`
    end
  
    def post(msg)
      Net::HTTP.post URI(@url),
                      { "channel" => "#{@channel}", "text" => "#{msg}"}.to_json,
                      "Content-Type" => "application/json",
                      "Authorization" => "Bearer #{@user_token}",
                      "as_user" => "true",
                      "username" => "AV-Relay"
    end
  end
end