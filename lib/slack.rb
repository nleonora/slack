require "slack/version"
require 'slack/sender'

module Slack
  class Error < StandardError; end
end
